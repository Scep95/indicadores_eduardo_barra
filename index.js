//importando librerias y modulos externos
const fs = require('fs');
const fetch = require('node-fetch');
const readline = require('readline');
const timestamp = require('time-stamp');

//definiendo un lector
var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

//arreglo de indicadores
var indicadores = [];
//objeto que obtiene los parametros de cada archivo
var archivo = new Object();
archivo.fecha = "";
archivo.dolar = "";
archivo.euro = "";
archivo.tasadesempleo = "";
var informacion = "";


// formato de la captura de tiempo
console.log(timestamp('DDMMYYYY HH:mm:ss'));
function opcionMenu(opcion) {
    switch (opcion) {
        case '1': require('./Data API.js');
            console.log('Pasando por opcion 1 (actualizar archivo)');

            break;
        case '2': require('./Files API.js');


            break;
        case '3': console.log('Pasando por opcion 3 (valor mas actual)');

            getFile('../Evaluacion1/datos/25092019-155256.ind').then(JSON.parse).then((datos) => { informacion = filtradoDatos(datos) }).catch(console.error);


            break;
        case '4':
            break;
        case '5':
            break;
        case '6': process.exit(0);
            break;
        default:
            console.log('Opcion no encontrada');
            leerOpcion(lector).then((opcion) => { opcionMenu(opcion); }).catch(console.error);
            break;
    }

}


//funcion que lee un archivo
const getFile = fileName => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

const filtradoDatos = json => {
    var info = "";
    for (var elem in json) {
        //console.log(elem);
        if (elem == 'euro' || elem == 'dolar' || elem == 'tasa_desempleo') {
            info += elem + ": " + json[elem].valor + "\n";

        }

        if (elem == 'fecha') {
            info += elem + ':' + json[elem] + "\n";
        }

    }

    console.log(info);
    return info;
}

//menu con promesas
const leerOpcion = (lectura) => {
    return new Promise((resolve, reject) => {
        console.log('Menú');
        console.log('1.- Actualizar Datos');
        console.log('2.- Promediar');
        console.log('3.- Mostrar valor más actual');
        console.log('4.- Mostrar minimo histórico');
        console.log('5.- Mostrar maximo histórico');
        console.log('6.- Salir');
        lectura.question('Escriba su opcion: ', opcion => {
            resolve(opcion);
        });
    });
}




const ultimoArchivo = () => {
    return new Promise((resolve, reject) => {
        var path = '../Evaluacion1/datos';
        var valores = [];
        while (fechas.length) {
            fechas.pop();
        }
        fs.readdir(path, (err, files) => {
            resolve(fil.getFile(path + '/' + files[files.length - 1]).then(JSON.parse).then((json) => {
                return new Promise((resolve, reject) => {
                    var fecha = json['fecha'];
                    var hora = json['hora'];
                    var data = '';
                    data += 'Fecha y hora de guardado: ' + fecha + ' ' + hora + '\nValor actual del Dolar: ' + json['dolar'].valor;
                    data += '\nValor actual del Euro: ' + json['euro'].valor + '\nValor actual de Tasa Desempleo: ' + json['tasa_desempleo'].valor;
                    resolve(data);
                });
            }).catch(console.error));
        });
    });
};
//--------------------------------------------------------------------------------------------

leerOpcion(lector).then((opcion) => { opcionMenu(opcion); }).catch(console.error);
console.log(informacion);


