const fs = require('fs');
const fetch = require('node-fetch');
const timestamp = require('time-stamp');

const actualizar = () => {
    fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then(filtradoDatos).then((datos) => {
        fs.writeFile
            ('./datos/' + String(timestamp('DDMMYYYY-HHmmss')) + '.ind', JSON.stringify(datos), (err) => { console.error(err) });
    }).catch(console.error);
    //https://api.github.com/users/mitocode21
}

const status = response => {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response);
    }
    return Promise.reject(new Error(response.statusText));
};

//funcion que convierte los resultado de la solicitud en formato json
const obtenerJson = response => {
    return response.json();
};

const filtradoDatos = json => {
    return new Promise((resolve, reject) => {
        //var json = JSON.parse(data);
        var fecha = timestamp('DD-MM-YY');
        var hora = timestamp('HH:mm:ss');
        var info = '{"fecha":' + JSON.stringify(fecha) + ',';
        info += '"hora":' + JSON.stringify(hora) + ',';
        for (var elem in json) {
            
            if (elem === 'dolar' || elem === 'euro') {
                info += JSON.stringify(elem) + ':' + JSON.stringify(json[elem]) + ',';
            }
            if (elem === 'tasa_desempleo') {
                info += JSON.stringify(elem) + ':' + JSON.stringify(json[elem]);
            }
        }
        info += '}';
        console.log(json);
        resolve(JSON.parse(info));
    });

}
module.exports = actualizar();