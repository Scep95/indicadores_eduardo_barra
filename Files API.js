const fs = require('fs');
const fetch = require('node-fetch');
const readline = require('readline');
const timestamp = require('time-stamp');
const carpetas = '../Evaluacion1/datos/';
var archivos = [];

var content;

let promesa = new Promise((res) => {
    fs.readdirSync(carpetas).forEach(file => {
        archivos.push(file);
    });
    res(archivos);

});



function obtenerPromedio(lector) {
    let promesa = new Promise((resolve) => {
        var suma = 0;
        var sumaE=0;
        var sumaD=0;
        for (var i = 0; i < archivos.length; i++) {
            if (i < archivos.length - 1) {
                fs.readFile('../Evaluacion1/datos/' + archivos[i], 'utf-8', (err, data) => {
                    suma = suma + JSON.parse(data).dolar.valor;
                    sumaE = sumaE + JSON.parse(data).euro.valor;
                    sumaD = sumaD + JSON.parse(data).tasa_desempleo.valor;

                });
            } else {
                fs.readFile('../Evaluacion1/datos/' + archivos[i], 'utf-8', (err, data) => {
                    suma = suma + JSON.parse(data).dolar.valor;
                    sumaE = sumaE + JSON.parse(data).euro.valor;
                    sumaD = sumaD + JSON.parse(data).tasa_desempleo.valor;
                    promedio = suma /i;
                    promedioE= sumaE/i;
                    promedioD= sumaD/i;
                  

                    resolve(console.log("Promedio de indicador Dolar:"+promedio+"\nPromedio de indicador Euro "+promedioE
                    +"\nPromedio indicador Tasa de desempleo:"+promedioD));

                });
            }
        }
    });
}


promesa.then((resultado) => {
    // console.log(resultado);
}
).then(() => { obtenerPromedio() }).catch(console.error);
module.exports = obtenerPromedio;